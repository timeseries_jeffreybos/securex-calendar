export default {
    monthResourceTableWidth: 220,
    monthCellWidth: 50,
    tableHeaderHeight: 55,
    headerEnabled: false,
    nonAgendaSlotMinHeight: 100,
    nonWorkingTimeHeadColor: '#aeaeae',
    nonWorkingTimeHeadBgColor: '',
    nonWorkingTimeBodyBgColor: '#eeeeee',
    startResizable: false,
    endResizable: false,
};
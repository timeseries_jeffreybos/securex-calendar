import React from 'react';
import moment from 'moment';
import 'moment/locale/nl-be'
import 'moment/locale/fr'



import Scheduler, {SchedulerData, ViewTypes, DATE_FORMAT} from 'securex-react-big-scheduler';
import 'securex-react-big-scheduler/lib/css/style.css'
import withDragDropContext from './withDnDContext'
import CalendarConfig from './CalendarConfig';
import classNames from "classnames";


class Calendar extends React.Component {
    constructor(props) {
        super(props);
        moment.locale(props.locale);

        const defaultDate = moment(props.events && props.events[0].start || new Date()).format(DATE_FORMAT);
        const { resources, events } = this.props;

        this.setItemHeight();

        this.behaviours = {
            isNonWorkingTimeFunc: this.isNonWorkingTime
        };
        let schedulerData = new SchedulerData(defaultDate, ViewTypes.Month, false, false, CalendarConfig, this.behaviours);
        schedulerData.setResources(resources);
        schedulerData.setEvents(events);

        this.state = {
            viewModel: schedulerData,
            headerSelected: '',
            selectedEvents: [],
            borderColor: props.widget._contextObj.get(props.widget.configSelectedBorder) || '2BBEC3',
            showDetails: props.widget._contextObj.get(props.widget.configShowDetail),
            loaderAppearanceTime: props.widget._contextObj.get(props.widget.configLoadTime),
            toggleMonths: props.widget._contextObj.get(props.widget.configSelectedBorder),
            labelTotal: props.widget._contextObj.get(props.widget.labelTotal) || 'Totaal',
            defaultDate: defaultDate,
            disableClick: false,
        };
    }

    setItemHeight() {
        if (this.props.resources.length > 1) {
            if(this.state && this.state.viewModel) {
                this.state.viewModel.setNonAgendaSlotMinHeight(75)
            } else {
                CalendarConfig.nonAgendaSlotMinHeight = 75;
            }

        } else {
            if(this.state && this.state.viewModel) {
                this.state.viewModel.setNonAgendaSlotMinHeight(100)
            } else {
                CalendarConfig.nonAgendaSlotMinHeight = 100;
            }
        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.cancelSelection === true && this.props.cancelSelection !== prevProps.cancelSelection) {
            this.cancelSelection();
        }

        if (this.props.resources.length !== prevProps.resources.length ||
            (this.props.resources.length > 0 && this.props.resources.length === prevProps.resources.length) && this.props.resources[0].id !== prevProps.resources[0].id) {
            console.log('====================CHANGE RESOURCES');
            this.setItemHeight();
            this.state.viewModel.setResources(this.props.resources);
            this.setState({
                viewModel: this.state.viewModel
            });
        }

        if (this.props.events !== prevProps.events) {
            console.log('====================CHANGE EVENTS');
            this.state.viewModel.setDate(moment(this.props.events && this.props.events[0].start).format(DATE_FORMAT));
            this.state.viewModel.setEvents(this.props.events);

            this.setState({
                viewModel: this.state.viewModel
            })
        }
    }

    setSelectedEvents = (start, end, resourceId) => {
        const selected = this.props.events.filter(event => {
            if (event.resourceId === resourceId || !resourceId) {
                const eventDate = moment(new Date(event.start)).format('YYYY-MM-DD');
                const startDate = moment(new Date(start)).format('YYYY-MM-DD');
                const endDate = moment(new Date(end)).format('YYYY-MM-DD');

                if (startDate <= eventDate && endDate >= eventDate && event.editable) {
                    return event;
                }
            }
        });

        let tempSelected = [...this.state.selectedEvents];

        selected.forEach((selectedItem) => {
            const index = tempSelected.indexOf(selectedItem);
            if(index === -1) {
                tempSelected.push(selectedItem);
            } else {
                tempSelected.splice(index, 1);
            }
        });

        if (this.props.widget._execMF && selected.length >= 1) {
            this.callSelectMicroflow(selected);
        }

        this.setState({
            selectedEvents: [...tempSelected]
        });
    };

    cancelSelection = () => {
        this.setState({
            selectedEvents: []
        })
    };

    callSelectMicroflow(selectedItems) {
        this.setState({disableClick: true});

        const selectedEvents = selectedItems.map(f => {
            f.mxObject.set('Selected', !f.mxObject.get(this.props.widget.eventSelected));
            return f.id;
        });
        const microflow = selectedEvents.length === 1 ? this.props.widget.onItemSelectMicroflow : this.props.widget.onItemsSelectMicroflow;

        this.props.widget._execMF(
            selectedEvents,
            microflow,
            (data) => {
                setTimeout(() => this.setState({ disableClick: false }), 1000);
                console.log('===============CalendarJS:eventItemClick:this.state.widget.onItemsSelectMicroflow Called================', data)
            }
        );
    }

    newEvent(schedulerData, slotId, slotName, start, end) {
            if(!this.state.disableClick) {
                this.setSelectedEvents(start, end, slotId);
            }
    }

    onHeaderClick = ({time}) => {
        this.setSelectedEvents(time, time, false);
    };

    nonAgendaCellHeaderTemplateResolver = (schedulerData, item, formattedDateItems, style) => {
        let datetime = schedulerData.localeMoment(item.time);
        let isCurrentDate = datetime.isSame(new Date(), 'day');
        let activeHeader = false;
        let dayOfWeek = datetime.weekday();

        const classes = classNames({
            'header-day': true,
            'current-day': isCurrentDate,
            'is-weekend': dayOfWeek === 5 || dayOfWeek === 6
        });

        return (
            <th width="48" key={item.time} className={classes} onClick={() => {
                this.onHeaderClick(item);
                activeHeader = !activeHeader;
            }}>
                {
                    formattedDateItems.map((formattedItem, index) => (
                        <div key={index}>
                            <span className="header-weekday">
                            {schedulerData.localeMoment(item.time).format('dd')}
                            </span>
                            <span className="header-weekday-number">
                            {schedulerData.localeMoment(item.time).format('DD')}
                            </span>
                        </div>
                    ))
                }
            </th>
        );
    };

    eventItemTemplateResolver = (schedulerData, event, bgColor) => {
        const {resources} = schedulerData;
        const {id, workingHours, textColor, title} = event;
        const divStyle = {
            backgroundColor: bgColor,
            color: textColor
        };

        const active = this.state.selectedEvents.findIndex(s => {
            return s.id === event.id;
        });

        const totalEventsPerDay = this.props.events.filter(e => {
            return e.start === event.start && e.resourceId === event.resourceId;
        });

        const headerSelected = moment(this.state.headerSelected).format('YYYY-MM-DD');
        const eventStart = moment(event.start).format('YYYY-MM-DD');

        const calendarEventClasses = classNames({
            'securex-calendar-event': true,
            'editable': event.editable,
            'non-editable': !event.editable,
            'moreThanFour': totalEventsPerDay.length > 4 && totalEventsPerDay[0].id === event.id,
            'selected': headerSelected === eventStart || active !== -1
        });

        return (
            <div className={calendarEventClasses} style={divStyle}
                 key={id}>
                <div>
                    <span className="event-working-hours">
                        {workingHours}
                        {totalEventsPerDay.length < 3 && <strong>{title}</strong>}
                    </span>

                </div>
            </div>
        );
    };

    isNonWorkingTime(schedulerData, time) {
        return false;
    }


    // Change cell color, we can use this for non working days
    getNonAgendaViewBodyCellBgColorFunc() {
        return '#000';
    }

    slotItemTemplateResolver = (schedularData, slot) => {
        const {resources} = schedularData;

        const resource = resources.filter(r => {
            return r.id === slot.slotId;
        })[0];


        let dropdown;
        if (this.props.actionMenus) {
            dropdown = this.props.actionMenus.filter(menu => {
                return menu.id === slot.slotId
            })[0];
        }


        return (
            <div className="securex-resource">
                <div className="securex-resource-left">
                    <span className="securex-resource-name">{resource.name}</span>
                    <span className="securex-resource-additional">{resource.additionalInfo}</span>
                </div>
                <div className="securex-resource-right">
                    {
                        resource.icon && <div className="securex-resource-warning">
                            <span
                                  className={`securex-resource-icon glyphicon ${resource.icon}`}>
                                <span className="securex-tooltip">
                                    {resource.iconMessage}
                                </span>
                            </span>
                        </div>
                    }
                    {
                        dropdown && dropdown.component
                    }
                </div>
            </div>
        )
    };

    eventItemPopoverTemplateResolver = (schedulerData, hoveredItem, title, start, end, statusColor) => {
        const {events} = schedulerData;
        const currentStartDate = moment(hoveredItem.start).format('YYYY-MM-DD');

        const eventsOfTheDay = events.filter(event => {
            const eventStartDate = moment(event.start).format('YYYY-MM-DD');
            return event.resourceId === hoveredItem.resourceId && eventStartDate === currentStartDate;
        });

        return (
            <div className="securex-popover">
                {this.workinghoursDescriptionPopoverTemplate(eventsOfTheDay)}
                {!!this.totalWorkingHoursOfTheDay(eventsOfTheDay) &&
                    <div className="securex-popover-total">
                        <span>{this.state.labelTotal}</span> <span
                        className="securex-popover-hours-total">{this.totalWorkingHoursOfTheDay(eventsOfTheDay).toFixed(2)}</span>
                    </div>
                }
            </div>
        );
    };

    workinghoursDescriptionPopoverTemplate(events) {
        return events.map((item, index) => (
            <div className="securex-popover-container" key={index}>
                <span className="securex-popover-longlabel">{item.longLabel}</span>
                <span className="securex-popover-hours">{item.workingHours}</span>
            </div>
        ));
    }

    totalWorkingHoursOfTheDay(events) {
        return events.map(event => {
            return parseFloat(event.workingHours.replace(/,/g, '.'))
        }).reduce((accumulator, currentValue) => {
            return accumulator + currentValue
        });

    }

    prevClick(event) {
    }

    nextClick(event) {
    }

    onViewChange(change) {
    }

    onSelectDate(schedulerData, date) {

    }

    render() {
        const {viewModel} = this.state;
        return (
            <Scheduler
                schedulerData={viewModel}
                prevClick={this.prevClick}
                nextClick={this.nextClick}
                onViewChange={this.onViewChange}
                onSelectDate={this.onSelectDate}
                eventItemTemplateResolver={this.eventItemTemplateResolver}
                nonAgendaCellHeaderTemplateResolver={this.nonAgendaCellHeaderTemplateResolver}
                eventItemPopoverTemplateResolver={this.eventItemPopoverTemplateResolver}
                slotItemTemplateResolver={this.slotItemTemplateResolver}
                newEvent={(schedulerData, slotId, slotName, start, end, type, item) => {
                    this.newEvent(schedulerData, slotId, slotName, start, end, type, item)
                }}
            />
        )
    }
}

export default withDragDropContext(Calendar);
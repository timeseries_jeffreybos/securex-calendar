import React from 'react';
import * as timeline from 'timeline-plus';
import startOfMonth from 'date-fns/start_of_month';
import endOfMonth from 'date-fns/end_of_month';


import addDays from 'date-fns/add_days';

class Timeline extends React.Component {
    state = {
        timeline: timeline,
        options: {
            start: startOfMonth(new Date()),
            end: endOfMonth(new Date()),
            timeAxis: {scale: 'day', step: 1},
            zoomable: false,
            moveable: false,
            horizontalScroll: false,
            orientation: {
                axis: 'top',
                item: 'bottom'
            },
            format: {
                minorLabels: {
                    day: 'DD',
                },
                majorLabels: {
                    day: '',
                }
            },
            showCurrentTime: false
        },
        groups: new timeline.DataSet([
            {id: 1, content: 'Steven Claus'},
            {id: 2, content: 'Bram Philips'},
            {id: 3, content: 'Frank van Paemel'},
        ]),
        items: new timeline.DataSet([
            {
                id: 1,
                group: 1,
                content: 'item 1',
                start: addDays(new Date(), 1),
                end: addDays(new Date(), 2),
                type: 'background'
            },

        ]),
    };

    constructor() {
        super();


        console.log(startOfMonth(new Date()))
        this.appRef = React.createRef();
    }


    componentDidMount() {
        this.timeline = new timeline.Timeline(this.appRef.current);
        this.timeline.setGroups(this.state.groups);
        this.timeline.setItems(this.state.items);
        this.timeline.setOptions(this.state.options);
    }

    render() {
        return <div>
            <div ref={this.appRef}></div>
        </div>
    }

}

export default Timeline;
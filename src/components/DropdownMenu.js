import React from 'react';
import './DropdownMenu.css';

class DropdownMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showMenu: false
        }
    }

    showMenu = (ev) => {
        ev.preventDefault();

        this.setState({
            showMenu: true
        }, () => {
            document.addEventListener('click', this.closeMenu);
        });
    };

    closeMenu = () => {
      this.setState({
          showMenu: false
      }, () => {
         document.removeEventListener('click', this.closeMenu);
      });
    };


    renderButton() {
        return this.props.items.map((item, index) => {
           return <button key={index} onClick={() => this.props.onActionClick(item)}>{item.name}</button>
        });
    }

    render() {
        return (
            <div className="securex-dropdown-action">
                <div className="securex-dropdown-button" onClick={this.showMenu}>
                    <span>.</span>
                    <span>.</span>
                    <span>.</span>
                </div>
                {
                    this.state.showMenu && this.props.items && <div className="securex-dropdown-menu">
                        {this.renderButton()}
                    </div>
                }
            </div>
        );
    }
}

export default DropdownMenu;
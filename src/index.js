import 'react-app-polyfill/ie11';
import 'core-js/es6/string';
import 'core-js/es6/object';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
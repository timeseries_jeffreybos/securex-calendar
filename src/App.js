import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import moment from 'moment';
import classNames from 'classnames';

import Calendar from "./components/Calendar";
import Select from "react-select";
import DropdownMenu from "./components/DropdownMenu";


if (typeof widget === "undefined") {
    var widget = {
        id: "Widget_Id_Widget1",
        mx: {},
        _execMF: false,
        _reactWidget: false,
        _contextObj: {
            get: () => {
                return ''
            }
        }
    }
}

class App extends React.Component {
    state = {
        widget: widget,
        resources: [],
        events: [],
        tempEvents: [],
        selectGroup: [],
        isLoadingResources: false,
        selectWidth: 300,
        actionMenus: [],
        inputValue: 'Alle',
        isDropdownSelection: false,
        resourcesSelected: 0,
        cancelSelection: false
    };

    constructor(props) {
        super(props);

        this.dropdownRef = React.createRef();
    }

    componentWillMount() {
        this.setState({
            widget: widget
        })
    }

    componentDidMount() {
        // Attach the react app view to the widget.
        if (this.state.widget._setReactWidget) {
            this.state.widget._setReactWidget(this);
        }

        window.addEventListener("resize", this.updateDimensions.bind(this));

        setTimeout(() => {
            this.updateDimensions();
        });

        this.prepareCalendar();
    }

    updateDimensions() {
        const resourceView = document.querySelector('.resource-view');

        if(resourceView) {
            this.setState({
                selectWidth: resourceView.clientWidth - 35
            })
        }
    }

    prepareCalendar(isUpdatingEvents = false) {
        if (!isUpdatingEvents) {
            this.getResources();
        } else {
            this.setState({
                events: [{
                    bgColor:"#A7DC33",
                    editable:true,
                    end:"2012-07-01 01:00",
                    id:"6558366957363s72307",
                    longLabel:"Feestdag",
                    mxObject: {},
                    nonWorkingDay:false,
                    resourceId:"70931s694131107818",
                    shortLabel:"F",
                    start:"2012-07-01 00:00",
                    textColor:"#FFFFFF",
                    title:"F",
                    workingHours:"7,60"
                }]
            });

            this.getEventItems();
        }

    }

    getResources() {
        if (this.state.widget._execMF) {
            this.state.widget._execMF(
                this.state.widget._contextObj,
                this.state.widget.contextDatasourceMf,
                (resources) => {
                    this.prepareResources(resources);
                    this.getEventItems();
                }
            );
        }
    }


    prepareResources(resources, isUpdatingEvents) {
        const arrayOfResources = [];
        const group = [];

        resources.forEach((resource, i) => {
            if (resource.get(this.state.widget.resourceType) === 'LEAF') {
                arrayOfResources.push({
                    mxObject: resource,
                    id: resource.getGuid(),
                    name: resource.get(this.state.widget.resourceName),
                    label: resource.get(this.state.widget.resourceName),
                    value: resource.get(this.state.widget.resourceName),
                    icon: resource.get(this.state.widget.resourceIcon),
                    iconMessage: resource.get(this.state.widget.resourceIconMessage),
                    group: resource.getReference('PrestationsModule.Resource_Resource'),
                    additionalInfo: resource.get(this.state.widget.resourceAdditional),
                    isSelected: resource.get(this.state.widget.resourceSelected),
                });

                if(resource.get(this.state.widget.resourceSelected)) {
                    this.getResourceActions(resource);
                }

            } else {
                group.push({
                    id: resource.getGuid(),
                    label: resource.get(this.state.widget.resourceName),
                    type: resource.get(this.state.widget.resourceType),
                    mxObject: resource
                })
            }
        });

        if (!isUpdatingEvents) {
            this.prepareDropdown(arrayOfResources, group);
        }

        this.setState({
            resources: [...arrayOfResources]
        });
    }

    callActionMicroflow = (action) => {
        if (this.state.widget._execMF) {
            this.state.widget._execMF(
                action.mxObaction.mxObjectject,
                this.state.widget.resourceExecuteAction,
                () => {
                    console.log('Action Executed')
                }
            )
        }
    };

    getResourceActions(resourceObj) {
        if (this.state.widget._execMF) {
            this.state.widget._execMF(
                resourceObj,
                this.state.widget.resourceGetActions,
                (actions) => {
                    const actionsArray = actions.map(action => {
                        return {
                            id: action.getGuid(),
                            mxObject: action,
                            name: action.get('Label'),
                            actionType: action.get('ActionType')
                        }
                    });

                    if (actionsArray && actionsArray.length > 0) {
                        this.setState({
                            actionMenus: [...this.state.actionMenus, {
                                id: resourceObj.getGuid(),
                                component: <DropdownMenu onActionClick={this.callActionMicroflow} items={actionsArray}/>
                            }]
                        })
                    }

                }
            );
        }
    }

    updateResources(resources) {
        this.prepareResources(resources, true);
        this.isDropdownSelection = false
    }

    prepareDropdown(resources, group) {
        let sorted = resources.sort((a, b) => {
            if (a.group < b.group) {
                return -1;
            }
            if (a.group > b.group) { return 1; }

            return 0;
        });

        group.forEach(g => {
            const index = sorted.findIndex(resource => {
                return g.id === resource.group;
            });
            resources.splice(index === -1 ? 0 : index, 0, g)
        });

        this.setState({
            selectGroup: [...resources]
        });
    }

    getEventItems = () => {
        if (this.state.widget._execMF) {
            this.state.widget._execMF(
                this.state.widget._contextObj,
                this.state.widget.microflowGetEventItems,
                (events) => this.prepareEvents(events)
            );
        }
    };

    prepareEvents(events) {
        let arrayOfEvents;

        if(!events.length) {
            arrayOfEvents = [];
        }

        arrayOfEvents = events.map(event => {
            return {
                mxObject: event,
                id: event.getGuid(),
                start: moment(event.get(this.state.widget.eventDate)).startOf('day').add(10, 'hour').format('YYYY-MM-DD HH:mm'),
                end: moment(event.get(this.state.widget.eventDate)).startOf('day').add(11, 'hour').format('YYYY-MM-DD HH:mm'),
                resourceId: event.get(this.state.widget.eventResourceId),
                title: event.get(this.state.widget.eventShortLabel),
                shortLabel: event.get(this.state.widget.eventShortLabel),
                longLabel: event.get(this.state.widget.eventLongLabel),
                workingHours: event.get(this.state.widget.eventTopLabel),
                bgColor: '#' + event.get(this.state.widget.eventColor),
                textColor: '#' + event.get(this.state.widget.eventTextColor),
                nonWorkingDay: event.get(this.state.widget.eventNonWorkingDay),
                editable: event.get(this.state.widget.eventEditable),
            };
        });

        this.setState({
            events: arrayOfEvents,
        });
    }

    onSelectChange(resource) {
        this.isDropdownSelection = true;

        if (this.state.widget._execMF) {
            resource.mxObject.set('Selected', true);
            this.state.widget._execMF(
                resource.mxObject,
                this.state.widget.onResourceSelectMicroflow,
                (resources) => {
                    this.updateResources(resources);
                    this.getEventItems();
                }
            );
        }
    }

    getSelectedResources() {
        return this.state.resources.filter(res => {
            return res.isSelected === true
        });
    }

    render() {
        const calendarClass = classNames({
            'securex-calendar': true,
            'hidden-detail': this.state.resources.length > 1
        });

        const Option = props => {
            const { innerProps, innerRef } = props;
            return (
                <article ref={innerRef} {...innerProps} className={"custom-option " + (props.data.type === "PARENT" ? 'custom-option-parent' : '')} >
                    <h4>{props.data.label}</h4>
                </article>
            );
        };

        const locale = mx ? mx.session.sessionData.locale.code : 'nl_BE'; // eslint-disable-line

        return (
            <div className={calendarClass}>
                <div ref={this.dropdownRef} className="securex-dropdown" style={{width: this.state.selectWidth}}>
                    <Select ref={(ref) => this.selectRef = ref}
                            onChange={(selected) => this.onSelectChange(selected)}
                            options={this.state.selectGroup}
                            components={{ Option }}/>
                </div>
                {this.state.events.length && this.state.widget ?
                    <Calendar widget={this.state.widget}
                              locale={locale}
                              cancelSelection={this.state.cancelSelection}
                              resources={this.getSelectedResources()}
                              actionMenus={this.state.actionMenus}
                              events={this.state.events}/> : null}

            </div>
        );
    }

    cancelSelection() {
        const cancelSelection = this.state.widget._contextObj.get(this.state.widget.contextCancel);
        this.setState({
            cancelSelection: cancelSelection
        })
    }

    refreshState(widget) {
        this.setState({widget: widget});

        if(!this.state.isDropdownSelection) {
            this.prepareCalendar(true)
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions.bind(this), false);
    }

    unmountMe() {
        ReactDOM.unmountComponentAtNode(document.getElementById(this.state.widget.id));
    }
}

export default App;
